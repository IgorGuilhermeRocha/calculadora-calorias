package com.igor.caloriescalculator.fragments.interfaces;

/**
 * @author Igor Guilherme Almeida Rocha
 * Interface usada para fazer a comunicação da activity com o dialog do datepicker
 */
public interface SetDateFromFragmentInterface {

    void setDate(String date);
}
